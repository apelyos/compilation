(* hw1.ml *)

#use "pc.ml";;

(* *)

type reg = Reg of int;;

type imm = Imm of int;;

type opcode = Add | Sub | Mul | Div | Mov;;

type inst =
  | InstRR of opcode * reg * reg
  | InstRI of opcode * reg * imm;;

type scm_op = ScmAdd | ScmSub | ScmMul | ScmDiv;;
  
type expr =
  | ScmInt of int
  | ScmOp of scm_op * expr list;;

type expr' =
  | Uninitialized
  | ScmInt' of int
  | ScmOp' of scm_op * expr' list;;

exception X_This_should_not_happen;;
exception X_Not_yet_implemented;;
exception X_Expression_includes_uninitialized_values;;
exception X_Cannot_parse_expr_string;;
exception X_Cannot_parse_assembly_program_string;;

module type PARSERS = sig
  val nt_assembly_program : char list -> inst list * char list 
  val nt_expr : char list -> expr * char list 
end;; (* end of signature PARSERS *)


module Parsers  : PARSERS  = struct 
  open PC;;
  
  let make_char_value base_char displacement =
    let base_char_value = Char.code base_char in
    fun ch -> (Char.code ch) - base_char_value + displacement;;

  let nt_digit_0_9 = pack (range '0' '9') (make_char_value '0' 0);;
  
  let nt_digit_1_9 = pack (range '0' '9') (make_char_value '0' 0);;
  
  let nt_nat =
    let nt = range '1' '9' in
    let nt = pack nt (make_char_value '0' 0) in
    let nt' = range '0' '9' in
    let nt' = pack nt' (make_char_value '0' 0) in
    let nt' = star nt' in
    let nt = caten nt nt' in
    let nt = pack nt (fun (d, ds) -> (d :: ds)) in
    let nt = pack nt (fun s -> List.fold_left (fun a b -> a * 10 + b) 0 s) in
    let nt' = char '0' in
    let nt'' = char '0' in
    let nt''' = range '0' '9' in
    let nt'' = caten nt'' nt''' in
    let nt' = diff nt' nt'' in
    let nt' = pack nt' (fun e -> 0) in
    let nt = disj nt nt' in
    nt;;

  let nt_register =
    let nt = char_ci 'r' in
    let nt = caten nt nt_nat in
    let nt = pack nt (fun (_r, n) -> (Reg n)) in
    nt;;

  let nt_int =
    let nt = char '-' in
    let nt = pack nt (fun e -> -1) in
    let nt' = char '+' in
    let nt' = pack nt' (fun e -> 1) in
    let nt = disj nt nt' in
    let nt = maybe nt in
    let nt = pack nt (function | None -> 1 | Some(mult) -> mult) in
    
    let nt' = range '0' '9' in
    let nt' = pack nt' (make_char_value '0' 0) in
    let nt' = plus nt' in
    let nt' = pack nt' (fun s -> List.fold_left (fun a b -> a * 10 + b) 0 s) in

    let nt = caten nt nt' in
    let nt = pack nt (fun (mult, n) -> (mult * n)) in
    nt;;

  let nt_imm = pack nt_int (fun n -> (Imm n));;

  let nt_opcode =
    let nt = word_ci "add" in
    let nt = pack nt (fun _ -> Add) in
    let nt' = word_ci "sub" in
    let nt' = pack nt' (fun _ -> Sub) in
    let nt'' = word_ci "mul" in
    let nt'' = pack nt'' (fun _ -> Mul) in
    let nt''' = word_ci "div" in
    let nt''' = pack nt''' (fun _ -> Div) in
    let nt'''' = word_ci "mov" in
    let nt'''' = pack nt'''' (fun _ -> Mov) in
    let nt = disj nt (disj nt' (disj nt'' (disj nt''' nt''''))) in
    nt;;

(* add your own code here after this comment *)
  let nt_scm_op = 
disj_list [(pack (char '+') (fun _ -> ScmAdd));
           (pack (char '-') (fun _ -> ScmSub));
	   (pack (char '*') (fun _ -> ScmMul));
	   (pack (char '/') (fun _ -> ScmDiv))];;

 
  let nt_scm_nat =  (pack nt_nat (fun i -> ScmInt i));;
  let nt_scm_int =  (pack nt_int (fun i -> ScmInt i));;

 let nt_space_star = star nt_whitespace;;

 let nt_wrap_s nt = 
   let nt = caten nt_space_star (caten nt nt_space_star) in
   let nt = pack nt (fun (_,(y,_)) -> y) in
   nt;;

  let nt_space_plus_b4 nt =
    let nt = caten (plus nt_whitespace) nt in
    let nt = pack nt (fun (_,y) -> y) in
    nt;;

  let nt_space_star_b4 nt =
    let nt = caten (star nt_whitespace) nt in
    let nt = pack nt (fun (_,y) -> y) in
    nt;;

  let nt_space_b4 nt = 
    let nt = caten nt_whitespace nt in
    let nt = pack nt (fun (_,y) -> y) in
    nt;;

  let clean_unary = function
    | ScmOp (ScmDiv, x::[]) -> ScmOp (ScmDiv, [ScmInt 1; x])
    | ScmOp (ScmMul, x::[]) -> ScmOp (ScmMul, [ScmInt 1; x])
    | ScmOp (ScmAdd, x::[]) -> ScmOp (ScmAdd, [ScmInt 0; x])
    | ScmOp (ScmSub, x::[]) -> ScmOp (ScmSub, [ScmInt 0; x])
    | x -> x;;

  let nt_expr_hlpr = 
    let rec foo () = 
      let nt1 = nt_space_star_b4 (char '(') in
      let nt2 = nt_space_star_b4 (char ')') in
      let ntr = (delayed foo) in
      let nto = nt_space_star_b4 nt_scm_op in
      let opt = disj (nt_space_plus_b4 nt_scm_int) ntr in
      let alt1 = caten nto (caten opt (star (disj ntr (nt_space_star_b4 nt_scm_int)))) in
      let alt1' = pack alt1 (fun (a,(b,c)) -> (ScmOp (a,b::c))) in
      let comb1 = caten nt1 (caten alt1'  nt2) in
      let nte1 = pack comb1 (fun (_,(x,_)) -> clean_unary x) in
      nte1 in
    foo ();;

  let nt_expr = 
    pack (caten nt_expr_hlpr nt_space_star) (fun (x,_) -> x);;

  let nt_assembly_inst =
    let opc = nt_space_star_b4 nt_opcode in
    let reg = nt_space_plus_b4 nt_register in
    let imm = pack (caten (char '$') nt_imm) (fun (_,x) -> x) in
    let imm' = nt_space_star_b4 imm in
    let sep = nt_space_star_b4 (char ',') in
    let inst_rr = caten opc (caten reg (caten sep reg)) in
    let inst_ri = caten opc (caten reg (caten sep imm')) in
    let c_rr = pack inst_rr (fun (x,(y,(_,z))) -> InstRR (x,y,z)) in
    let c_ri = pack inst_ri (fun (x,(y,(_,z))) -> InstRI (x,y,z)) in
    let nt = disj c_rr c_ri in
    nt ;;

  let nt_assembly_program = 
    let lf = plus (char '\n') in
    let cdr = caten lf nt_assembly_inst in
    let cdr' = pack cdr (fun (_,x) -> x) in
    let prg = caten nt_assembly_inst (star cdr') in
    let nt = pack prg (fun (x,y) -> x::y) in
    nt;;


end;; (* end of struct Parsers *)

module type FULL_CYCLE = sig
  val compile_arith : expr -> inst list
  val assembly_program_to_string : inst list -> string
  val decompile_assembly_program : inst list -> expr'
  val expr'_to_string : expr' -> string
  val full_cycle : string -> string
end;; (* end of signature FULL_CYCLE *)

module Full_Cycle : FULL_CYCLE  = struct

let apply_append s = List.fold_right (@) s [];;

let find_max_register insts =  
  1 + (List.fold_right max 
		       (List.map
			  (function
			    | InstRI(_, (Reg i), _) -> i
			    | InstRR(_, (Reg i), (Reg j)) -> (max i j))
			  insts)
		       0);;

(* add your own code after this *)

  let rec compile_arith_ToRegister_test regNumber expr firstBool =
    let rec compile_arith_ToRegister_withList_test op regNumber exprList isFirst =
      match exprList with
      | [] -> []

      | car :: cdr ->
        match car with
        | ScmInt n ->
          if isFirst = true then
            [InstRI (Mov, Reg regNumber, Imm n)] @ (compile_arith_ToRegister_withList_test op regNumber cdr false)
          else                                                   
            [InstRI (op, Reg regNumber, Imm n)] @ (compile_arith_ToRegister_withList_test op regNumber cdr false)

        | ScmOp (scmOp,exprList) ->
          if isFirst = true then 
            compile_arith_ToRegister_test regNumber car true @  (compile_arith_ToRegister_withList_test op regNumber cdr false)                                                    
          else
             compile_arith_ToRegister_test (regNumber+1) car true
             @ [InstRR (op, Reg regNumber, Reg (regNumber+1))]
             @ (compile_arith_ToRegister_withList_test op regNumber cdr false) in

    match expr with
    | ScmInt n -> [InstRI (Mov, Reg regNumber, Imm n)]
    | ScmOp (op,exprList) ->
      match op with 
      | ScmAdd -> compile_arith_ToRegister_withList_test Add regNumber exprList firstBool
      | ScmSub -> compile_arith_ToRegister_withList_test Sub regNumber exprList firstBool
      | ScmMul -> compile_arith_ToRegister_withList_test Mul regNumber exprList firstBool
      | ScmDiv -> compile_arith_ToRegister_withList_test Div regNumber exprList firstBool ;;

let compile_arith expr = compile_arith_ToRegister_test 0 expr true;;

let string_of_assop = function
  | Add -> "add"
  | Sub -> "sub"
  | Mul -> "mul"
  | Div -> "div"
  | Mov -> "mov";;


let inst_to_string = function
  | InstRR(op,Reg(x),Reg(y)) -> 
    (string_of_assop op) ^ " r" ^ (string_of_int x) ^ ", r" ^  (string_of_int y)
  | InstRI(op,Reg(r),Imm(n)) -> (string_of_assop op) ^ " r" ^ (string_of_int r) ^ ", $" ^  (string_of_int n)

let assembly_program_to_string prg = 
  String.concat "\n" (List.map inst_to_string prg);;

let conv_ass_op = function
  | Add -> ScmAdd
  | Sub -> ScmSub
  | Mul -> ScmMul
  | Div -> ScmDiv
  | Mov -> raise X_This_should_not_happen ;;

let conv_ass_im = (function Imm(n) -> ScmInt'(n));;

let rec decompile_assembly_program_hlpr regArr = 
  function
  | [] -> 
    regArr.(0)
  | InstRI(Mov,Reg(r),n)::tail ->  
    begin 
      regArr.(r) <- (conv_ass_im n);
      decompile_assembly_program_hlpr regArr tail;
    end
  | InstRI(op,Reg(r),n)::tail -> 
    begin
      regArr.(r) <- (ScmOp'((conv_ass_op op), regArr.(r)::[(conv_ass_im n)] ));
      decompile_assembly_program_hlpr regArr  tail;
    end
  | InstRR(op,Reg(r),Reg(s))::tail ->  
    begin
      regArr.(r) <- (ScmOp'((conv_ass_op op), regArr.(r)::[regArr.(s)] ));
      decompile_assembly_program_hlpr regArr tail;
    end
;;
  
let decompile_assembly_program exp = 
  decompile_assembly_program_hlpr (Array.make (find_max_register exp) Uninitialized) exp;;

let string_of_scmop = function
  | ScmAdd -> "+"
  | ScmSub -> "-"
  | ScmMul -> "*"
  | ScmDiv -> "/"

let rec expr'_to_string = function
  | Uninitialized -> 
    raise X_Expression_includes_uninitialized_values
  | ScmInt'(n) -> 
    string_of_int n
  | ScmOp'(op, expr_list) -> 
    "(" ^ (string_of_scmop op) ^ " " ^ (String.concat " " (List.map expr'_to_string expr_list)) ^ ")"

(* do not add your own code after this *)

let full_cycle string =
  try (match (Parsers.nt_expr (string_to_list string)) with
  | (expr, []) ->
    (try (match (Parsers.nt_assembly_program
		   (string_to_list
		      (assembly_program_to_string
			 (compile_arith expr)))) with 
    | (insts, []) ->
      (expr'_to_string (decompile_assembly_program insts))
    | _ -> raise X_Cannot_parse_assembly_program_string)
     with PC.X_no_match -> raise X_Cannot_parse_assembly_program_string)
  | _ -> raise X_Cannot_parse_expr_string)
  with PC.X_no_match -> raise X_Cannot_parse_expr_string;;

end;; (* end of struct Full_Cycle *)
  
(* end of input *)

