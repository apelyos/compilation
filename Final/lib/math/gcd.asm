/* by: yos
  takes 2 integers and compute the gcd
*/
  
 
 L_GCD:
  PUSH(FP);
  MOV(FP,SP);
  
  PUSH(R1);
  PUSH(R2);
  PUSH(R3);
  
  MOV(R1, FPARG(0)); /* U */
  MOV(R2, FPARG(1)); /* V */
  /*printf("GCD: R0: %d R1: %d, R2: %d R3: %d, R4: %d\n\n", R0, R1, R2, R3, R4);*/
  PUSH(R1);
  CALL(ABS);
  DROP(1);
  MOV(R1,R0);
  
  PUSH(R2);
  CALL(ABS);
  DROP(1);
  MOV(R2,R0);
  
  L_GCD_ITER:
  CMP(R2, IMM(0));
  JUMP_EQ(L_GCD_ITER_EXIT);
  MOV(R3, R1); /*t= r1*/
  MOV(R1, R2); /*r1 = r2*/
  REM(R3, R2); /* T = T mod r2 */
  MOV(R2, R3); /*R2 = T*/
  
  JUMP(L_GCD_ITER);  
  
  L_GCD_ITER_EXIT:
  MOV(R0, R1);
  
  POP(R3);
  POP(R2);
  POP(R1);
  
  POP(FP);
  RETURN;
  
 
 
 
 
 
 
