/* PROGRAMMER: NOT MAYER, BUT: YOS APEL
 */

 L_CONS:
  PUSH(FP);
  MOV(FP, SP);
  
  CMP(FPARG(1),IMM(2));
  JUMP_NE(L_CONS_ERR_ARGS);
  MOV(R0,FPARG(2));
  
  PUSH(FPARG(3));
  PUSH(FPARG(2));
  CALL(MAKE_SOB_PAIR);
  DROP(2);
  
  POP(FP);
  RETURN;
  
L_CONS_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (cons)",R0);
  return 1;
  

  
L_CONS_EXIT:
