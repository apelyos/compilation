/* (string-set! str pos char) */
L_STRING_SET:
	PUSH(FP);
	MOV(FP, SP);
	
	CMP(FPARG(1), IMM(3));
	JUMP_NE(L_STRING_SET_IncorrectNumberOfArgs);
	
	MOV(R1, FPARG(2));
	CMP(INDD(R1, 0), IMM(T_STRING)); 
	JUMP_NE (L_STRING_SET_IncorrectNotAString);
	
	MOV(R2, FPARG(3));
	CMP(INDD(R2, 0), IMM(T_INTEGER));
	JUMP_NE(L_STRING_SET_IncorrectIndex);
	
	MOV(R4, FPARG(4));
	CMP(INDD(R4, 0), IMM(T_CHAR));
	JUMP_NE(L_STRING_SET_IncorrectNotAChar);
	MOV(R4,INDD(R4,1));
	
	MOV(R2, INDD(R2, 1));
	CMP(R2, INDD(R1, 1));
	JUMP_GE(L_STRING_SET_OutOfRange);
	ADD(R2, IMM(2)) ; 
	MOV(INDD(R1, R2), R4);
	
	
	MOV(R0, IMM(CONST_VOID_ADDR));

	POP(FP);
	RETURN;
	
L_STRING_SET_IncorrectNumberOfArgs:
	SHOW("string-set! called with incorrect # of args!\n", R0);
	exit(1);
L_STRING_SET_IncorrectNotAString:
	SHOW("string-set! was called with a non-string!\n", R0);
	exit(1);
L_STRING_SET_IncorrectNotAChar:
	SHOW("string-set! was called with a non-char!\n", R0);
	exit(1);	
L_STRING_SET_IncorrectIndex:
	SHOW("string-set! was called with a non-integer index!\n", R0);
	exit(1);	
L_STRING_SET_OutOfRange:
	SHOW("string-set! out of range!\n", R0);
	exit(1);
	