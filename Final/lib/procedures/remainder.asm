/*YOSSS
 */ 
 
 L_REMAINDER:
  PUSH(FP);
  MOV(FP,SP);
  
  CMP(FPARG(1),IMM(2));
  JUMP_NE(L_REMAINDER_ERR_ARGS);
  
  MOV(R1,FPARG(2));
  CMP(ADDR(R1),IMM(T_INTEGER));
  JUMP_NE(L_REMAINDER_ERR_NOT);
  
  MOV(R2,FPARG(3));
  CMP(ADDR(R2),IMM(T_INTEGER));
  JUMP_NE(L_REMAINDER_ERR_NOT);
  
  MOV(R1,INDD(R1,1));
  MOV(R2,INDD(R2,1));
  
  REM(R1,R2);
  
  PUSH(R1);
  CALL(MAKE_SOB_INTEGER);
  DROP(1);
  
  
  POP(FP);
  RETURN;
  
 L_REMAINDER_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (remainder)",R0);
  return 1; 
 
 L_REMAINDER_ERR_NOT:  
  SHOW("Exception: remainder : not an int",R0);
  return 1; 
 
