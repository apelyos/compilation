/* PROGRAMMER: NOT MAYER, BUT: YOS APEL
 */

 L_CAR:
  PUSH(FP);
  MOV(FP, SP);
  
  CMP(FPARG(1),IMM(1));
  JUMP_NE(L_CAR_ERR_ARGS);
  MOV(R0,FPARG(2));
  CMP(INDD(R0,0),IMM(T_PAIR));
  JUMP_NE(L_CAR_ERR_NOT_PAIR);
  MOV(R0,INDD(R0,1));
  
  POP(FP);
  RETURN;
  
L_CAR_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (car)",R0);
  return 1;
  
L_CAR_ERR_NOT_PAIR:
  SHOW("Exception in car: not a pair",R0);
  return 1;
  
L_CAR_EXIT:
