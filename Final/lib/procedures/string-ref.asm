/* (string-ref vec pos) */
L_STRING_REF:
	PUSH(FP);
	MOV(FP, SP);
	
	CMP(FPARG(1), IMM(2));
	JUMP_NE(L_STRING_REF_IncorrectNumberOfArgs);
	MOV(R1, FPARG(2));
	CMP(INDD(R1, 0), IMM(T_STRING)); 
	JUMP_NE (L_STRING_REF_IncorrectNotAVector);
	MOV(R2, FPARG(3));
	CMP(INDD(R2, 0), IMM(T_INTEGER));
	JUMP_NE(L_STRING_REF_IncorrectIndex);
	MOV(R2, INDD(R2, 1));
	CMP(R2, INDD(R1, 1));
	JUMP_GE(L_STRING_REF_OutOfRange);
	ADD(R2, IMM(2)) ; 
	MOV(R1, INDD(R1, R2));
	
	PUSH(R1);
	CALL(MAKE_SOB_CHAR);
	DROP(1);

	POP(FP);
	RETURN;
	
L_STRING_REF_IncorrectNumberOfArgs:
	SHOW("string-ref called with incorrect # of args!\n", R0);
	exit(1);
L_STRING_REF_IncorrectNotAVector:
	SHOW("string-ref was called with a non-vector!\n", R0);
	exit(1);
L_STRING_REF_IncorrectIndex:
	SHOW("string-ref was called with a non-integer index!\n", R0);
	exit(1);	
L_STRING_REF_OutOfRange:
	SHOW("string-ref out of range!\n", R0);
	exit(1);
	