/* (make-vector k [object]) */
L_MAKE_VECTOR:
	PUSH(FP);
	MOV(FP, SP);
	
	CMP(FPARG(1), IMM(1));
	JUMP_LT(L_MAKE_VECTOR_IncorrectNumberOfArgs);
	MOV(R1, FPARG(2));
	CMP(INDD(R1, 0), IMM(T_INTEGER)); 
	JUMP_NE (L_MAKE_VECTOR_IncorrectIndex);
	MOV(R1,INDD(R1,1)); /* size arg */
	
	CMP(FPARG(1),IMM(1));
	JUMP_EQ(L_MAKE_VECTOR_ARG1);
	
	CMP(FPARG(1),IMM(2));
	JUMP_EQ(L_MAKE_VECTOR_ARG2);
	
	CMP(FPARG(1),IMM(2));
	JUMP_GT(L_MAKE_VECTOR_IncorrectNumberOfArgs);
	
	/* CASE OF 1 ARG */
	L_MAKE_VECTOR_ARG1:
	
	PUSH(IMM(0));
	CALL(MAKE_SOB_INTEGER);
	DROP(1);
	MOV(R2,R0);
	JUMP(L_MAKE_VECTOR_ARG2_CONT);
	
	/* CASE OF 2 ARG */
	L_MAKE_VECTOR_ARG2:
	MOV(R2, FPARG(3)); /* VAL */
	L_MAKE_VECTOR_ARG2_CONT:
	
	MOV(R4, IMM(0)); /* counter */
	L_MAKE_VECTOR_LOOP1:
	CMP(R4, R1);
	JUMP_EQ(L_MAKE_VECTOR_LOOP1_EXIT);
	PUSH(R2);
	INCR(R4);
	JUMP(L_MAKE_VECTOR_LOOP1);
	L_MAKE_VECTOR_LOOP1_EXIT:
	PUSH(R1);
	CALL(MAKE_SOB_VECTOR);
	DROP(R1+1);

	POP(FP);
	RETURN;
	
L_MAKE_VECTOR_IncorrectNumberOfArgs:
	SHOW("make-vector called with incorrect # of args!\n", R0);
	exit(1);
L_MAKE_VECTOR_IncorrectIndex:
	SHOW("make-vector was called with a non-integer index!\n", R0);
	exit(1);	