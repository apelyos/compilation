/*
  (RESPECT TO YOS'S IDEA)
  PROGRAMMER: NOT MAYER, BUT: YUVAL MAYMON
 */ 
 
 L_RATIONAL:
  PUSH(FP);
  MOV(FP,SP);
  
  CMP(FPARG(1),IMM(1));
  JUMP_NE(L_RATIONAL_ERR_ARGS);
  PUSH(FPARG(2));
  CALL(IS_SOB_FRACTION);
  DROP(1);
  MOV(R1,R0);
  
  PUSH(FPARG(2));
  CALL(IS_SOB_INTEGER);
  DROP(1);
  OR(R0,R1);
  
  CALL(TRUE_OR_FALSE_IN_R0);
  
  
  POP(FP);
  RETURN;
  
 L_RATIONAL_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (rational?)",R0);
  return 1; 
 
 
 
