/*
  yos
 */ 
 
 L_CHAR_TO_INT:
  PUSH(FP);
  MOV(FP,SP);
  
  CMP(FPARG(1),IMM(1));
  JUMP_NE(L_CHAR_TO_INT_ERR_ARGS);
  MOV(R1, FPARG(2));
  CMP(ADDR(R1),IMM(T_CHAR));
  JUMP_NE(L_CHAR_TO_INT_NOT_CHAR);
  
  MOV(R1,INDD(R1,1));
  PUSH(R1);
  CALL(MAKE_SOB_INTEGER);
  DROP(1);
  
  POP(FP);
  RETURN;
  
 L_CHAR_TO_INT_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (char->int)",R0);
  return 1; 

L_CHAR_TO_INT_NOT_CHAR:
  SHOW("Exception: char->int : not a char", R1);
  return 1; 