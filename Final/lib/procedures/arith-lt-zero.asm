/*
  yoS
  
 */ 
 
 L_ARITH_LT_ZERO:
  PUSH(FP);
  MOV(FP,SP);

  CMP(FPARG(1),IMM(1));
  JUMP_NE(L_ARITH_LT_ZERO_ERR_ARGS);
  
  MOV(R0, FPARG(2));
  
  CMP(ADDR(R0), IMM(T_INTEGER));
  JUMP_EQ(L_ARITH_LT_ZERO_CONT);
  
  CMP(ADDR(R0), IMM(T_FRACTION));
  JUMP_NE(L_ARITH_LT_ZERO_PARAM_IN_NOT_A_NUMBER);
  
  L_ARITH_LT_ZERO_CONT:
  
  MOV(R0,INDD(R0,1));
  CMP(R0,IMM(0));
  JUMP_LT(L_ARITH_LT_ZERO_TRUE);
  
   L_ARITH_LT_ZERO_FALSE:
 MOV(R0,IMM(0));
 JUMP(L_ARITH_LT_ZERO_CONTINUE);
 
 L_ARITH_LT_ZERO_TRUE:
  MOV(R0,IMM(1));
  
  

  
 L_ARITH_LT_ZERO_CONTINUE:
  CALL(TRUE_OR_FALSE_IN_R0);
 
  POP(FP);
  RETURN;
  
 L_ARITH_LT_ZERO_PARAM_IN_NOT_A_NUMBER:
  SHOW("Exception: the parameter in not a number", R0);
  return 1;
  
 L_ARITH_LT_ZERO_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (lt-zero?)",R0);
  return 1; 
 
 
 
 
 
 
 
