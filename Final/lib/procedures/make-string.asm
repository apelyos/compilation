/*
  Yos
 */ 
 
 L_MAKE_STRING:
  PUSH(FP);
  MOV(FP,SP);
  
  CMP(FPARG(1),IMM(1));
  JUMP_EQ(L_MAKE_STRING_ARG1);
  
  CMP(FPARG(1),IMM(2));
  JUMP_NE(L_MAKE_STRING_ERR_ARGS);
  
  PUSH(FPARG(3));
  CALL(IS_SOB_CHAR);
  DROP(1);

  CMP(R0, IMM(1));
  JUMP_NE(L_MAKE_STRING_INCORRECT);
  
  MOV(R1,FPARG(3));
  MOV(R1, INDD(R1,1)); /* R1 holds the char */
  
  JUMP(L_MAKE_STRING_CONT);
  
  L_MAKE_STRING_ARG1:
  MOV(R1, IMM(0));
  
  L_MAKE_STRING_CONT:
  PUSH(FPARG(2));
  CALL(IS_SOB_INTEGER);
  DROP(1);
  
  CMP(R0, IMM(1));
  JUMP_NE(L_MAKE_STRING_INCORRECT);
  
  MOV(R2,FPARG(2)); 
  MOV(R2,INDD(R2,1)); /* R2 holds the number */
  
  CMP(R2, IMM(0));
  JUMP_LT(L_MAKE_STRING_INCORRECT);  
  
  MOV(R3, IMM(0));
  
  L_MAKE_STRING_LOOP:
  CMP(R3, R2);
  JUMP_EQ(L_MAKE_STRING_LOOP_EXIT);
  PUSH(R1);
  INCR(R3);
  JUMP(L_MAKE_STRING_LOOP);
  
  L_MAKE_STRING_LOOP_EXIT:
  
  PUSH(R2);
  CALL(MAKE_SOB_STRING);
  DROP(R2+1);
  
  
  POP(FP);
  RETURN;
  
 L_MAKE_STRING_INCORRECT:
  SHOW("Error: not a valid argument for call make-string",R0);
  return 1; 
  
 L_MAKE_STRING_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (make-string)",R0);
  return 1; 
