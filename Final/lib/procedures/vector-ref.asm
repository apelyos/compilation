/* (vector-ref vec pos) */
L_VECTOR_REF:
	PUSH(FP);
	MOV(FP, SP);
	
	CMP(FPARG(1), IMM(2));
	JUMP_NE(L_VECTOR_REF_IncorrectNumberOfArgs);
	MOV(R1, FPARG(2));
	CMP(INDD(R1, 0), IMM(T_VECTOR)); 
	JUMP_NE (L_VECTOR_REF_IncorrectNotAVector);
	MOV(R2, FPARG(3));
	CMP(INDD(R2, 0), IMM(T_INTEGER));
	JUMP_NE(L_VECTOR_REF_IncorrectIndex);
	MOV(R2, INDD(R2, 1));
	CMP(R2, INDD(R1, 1));
	JUMP_GE(L_VECTOR_REF_OutOfRange);
	ADD(R2, IMM(2)) ; 
	MOV(R0, INDD(R1, R2));

	POP(FP);
	RETURN;
	
L_VECTOR_REF_IncorrectNumberOfArgs:
	SHOW("vector-ref called with incorrect # of args!\n", R0);
	exit(1);
L_VECTOR_REF_IncorrectNotAVector:
	SHOW("vector-ref was called with a non-vector!\n", R0);
	exit(1);
L_VECTOR_REF_IncorrectIndex:
	SHOW("vector-ref was called with a non-integer index!\n", R0);
	exit(1);	
L_VECTOR_REF_OutOfRange:
	SHOW("vector-ref out of range!\n", R0);
	exit(1);
	