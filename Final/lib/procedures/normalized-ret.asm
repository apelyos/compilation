L_NORM_RET:
  /* norm minus */
  CMP(R1,IMM(0));
  JUMP_EQ(L_NORM_RET_RET_INT);
  /*printf("RET: R0: %d R1: %d, R2: %d R3: %d, R4: %d\n\n", R0, R1, R2, R3, R4);*/
  
  /* minimize frac */
  PUSH(R1);
  PUSH(R2);
  CALL(L_GCD);
  DROP(2);
  
  CMP(R0,IMM(0));
  JUMP_EQ(L_NORM_RET_DIV_0);
  DIV(R1,R0);
  DIV(R2,R0);
  L_NORM_RET_DIV_0:
  
  /* norm minus */
  CMP(R2,IMM(0));
  JUMP_GT(L_NORM_RET_CH_FRAC);
  MUL(R1,IMM(-1));
  MUL(R2,IMM(-1));
  
  L_NORM_RET_CH_FRAC:
  
  /* check if frac */
  CMP(R2,IMM(1));
  JUMP_EQ(L_NORM_RET_RET_INT);
  
  PUSH(R2);
  PUSH(R1);
  CALL(MAKE_SOB_FRACTION);
  DROP(2);
  JUMP(L_NORM_RET_EXIT);
  
  L_NORM_RET_RET_INT:
  PUSH(R1);
  CALL(MAKE_SOB_INTEGER);
  DROP(1);
  
  L_NORM_RET_EXIT:
  RETURN;