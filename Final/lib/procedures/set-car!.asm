/*
  (RESPECT TO YOS'S IDEA)
  PROGRAMMER: NOT MAYER, BUT: YUVAL MAYMON
 */ 
 
  L_SET_CAR:
  PUSH(FP);
  MOV(FP, SP);
  
  CMP(FPARG(1),IMM(2));
  JUMP_NE(L_SET_CAR_ERR_ARGS);
  MOV(R0,FPARG(2));
  CMP(INDD(R0,0),IMM(T_PAIR));
  JUMP_NE(L_SET_CAR_ERR_NOT_PAIR);
    
  MOV(INDD(R0,1), FPARG(3));  
  MOV(R0, IMM(T_VOID));

  POP(FP);
  RETURN;
  
L_SET_CAR_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (set_car!)",R0);
  return 1;
  
L_SET_CAR_ERR_NOT_PAIR:
  SHOW("Exception in (set-car!): not a pair",R0);
  return 1;
  
L_SET_CAR_EXIT:
