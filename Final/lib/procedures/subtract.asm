 
 L_SUBTRACT:
  PUSH(FP);
  MOV(FP,SP);

  CALL(L_NORM_GET);
  
  MUL(R1,R4);
  MUL(R3,R2);
  MUL(R2,R4);
  SUB(R1,R3);
  
  CALL(L_NORM_RET);
  
  POP(FP);
  RETURN;
  

 