/*
  YOS
 */ 
 
 L_SYM_TO_STR:
  PUSH(FP);
  MOV(FP,SP);

  CMP(FPARG(1),IMM(1));
  JUMP_NE(L_SYM_TO_STR_ERR_ARGS);
  PUSH(FPARG(2));
  CALL(IS_SOB_SYMBOL);
  DROP(1);
  
  CMP(R0,IMM(1));
  JUMP_NE(L_SYM_TO_STR_ERR_NOT_SYM);
  
  MOV(R1,FPARG(2));
  MOV(R0,INDD(R1,1));
  
  POP(FP);
  RETURN;
  
 L_SYM_TO_STR_ERR_NOT_SYM:
  SHOW("Error: Parameter must be a symbol",R0);
  return 1;
  
 L_SYM_TO_STR_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (symbol->string)",R0);
  return 1; 
 
 
 
 
