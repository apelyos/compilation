/*
  YOS
 */ 
 
 L_STRING_LEN:
  PUSH(FP);
  MOV(FP,SP);

  CMP(FPARG(1),IMM(1));
  JUMP_NE(L_STRING_LEN_ERR_ARGS);
  PUSH(FPARG(2));
  CALL(IS_SOB_STRING);
  DROP(1);
  
  CMP(R0,IMM(1));
  JUMP_NE(L_STRING_LEN_ERR_NOT_STRING);
  
  MOV(R1,FPARG(2));
  PUSH(INDD(R1,1));
  CALL(MAKE_SOB_INTEGER);
  DROP(1);
  
  POP(FP);
  RETURN;
  
 L_STRING_LEN_ERR_NOT_STRING:
  SHOW("Error: Parameter must be a string",R0);
  return 1;
  
 L_STRING_LEN_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (string-length)",R0);
  return 1; 
 
