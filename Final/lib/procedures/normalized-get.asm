L_NORM_GET:
  CMP(FPARG(1),IMM(2));
  JUMP_NE(L_NORM_GET_ERR_ARGS);
  
  /* first arg */
  MOV(R0, FPARG(2));
  
  CMP(ADDR(R0), IMM(T_FRACTION));
  JUMP_NE(L_NORM_GET_CONT1);
  MOV(R1, INDD(R0,1));
  MOV(R2, INDD(R0,2));
  JUMP(L_NORM_GET_PAR2);
  
  L_NORM_GET_CONT1:
  CMP(ADDR(R0), IMM(T_INTEGER));
  JUMP_NE(L_NORM_GET_PARAM_IN_NOT_A_NUMBER);
  MOV(R1, INDD(R0,1));
  MOV(R2, IMM(1));
  JUMP(L_NORM_GET_PAR2);
  
  L_NORM_GET_PAR2:
  /* second arg */
  MOV(R0, FPARG(3));
  CMP(ADDR(R0), IMM(T_FRACTION));
  JUMP_NE(L_NORM_GET_CONT2);
  MOV(R3, INDD(R0,1));
  MOV(R4, INDD(R0,2));
  RETURN;
  
  L_NORM_GET_CONT2:
  CMP(ADDR(R0), IMM(T_INTEGER));
  JUMP_NE(L_NORM_GET_PARAM_IN_NOT_A_NUMBER);
  MOV(R3, INDD(R0,1));
  MOV(R4, IMM(1));
  RETURN;
  
  
   L_NORM_GET_PARAM_IN_NOT_A_NUMBER:
  SHOW("Exception: the parameter is not a number", R0);
  return 1;
  
 L_NORM_GET_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in arith call",R0);
  return 1; 
 