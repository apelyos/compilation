/* PROGRAMMER: NOT MAYER, BUT: YOS APEL
 */

 L_EQ:
  PUSH(FP);
  MOV(FP, SP);
  
  CMP(FPARG(1),IMM(2));
  JUMP_NE(L_EQ_ERR_ARGS);
  MOV(R0,FPARG(2));
  MOV(R1,FPARG(3));
  CMP(R0, R1);
  JUMP_EQ(L_EQ_SAME);
  
  CMP(INDD(R0,0),IMM(T_SYMBOL));
  JUMP_NE(L_EQ_DIFF);
  
  CMP(INDD(R1,0),IMM(T_SYMBOL));
  JUMP_NE(L_EQ_DIFF);
  
  CMP(INDD(R0,1),INDD(R1,1));
  JUMP_NE(L_EQ_DIFF);
  
L_EQ_SAME:  
  MOV(R0, CONST_TRUE_ADDR);
  POP(FP);
  RETURN;
  
L_EQ_DIFF:  
  MOV(R0, CONST_FALSE_ADDR);
  POP(FP);
  RETURN;
  
L_EQ_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (eq?)",R0);
  return 1;
  
L_EQ_EXIT:
