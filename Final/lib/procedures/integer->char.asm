/*
  yos
 */ 
 
 L_INTEGER_TO_CHAR:
  PUSH(FP);
  MOV(FP,SP);
  
  CMP(FPARG(1),IMM(1));
  JUMP_NE(L_INTEGER_TO_CHAR_ERR_ARGS);
  MOV(R1,FPARG(2));
  CMP(ADDR(R1),IMM(T_INTEGER));
  JUMP_NE(L_INTEGER_TO_CHAR_NOT_INT);
  MOV(R1,INDD(R1,1));
  
  PUSH(R1);
  CALL(MAKE_SOB_CHAR);
  DROP(1);
  
  POP(FP);
  RETURN;
  
 L_INTEGER_TO_CHAR_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (integer->char)",R0);
  return 1; 
 
 L_INTEGER_TO_CHAR_NOT_INT:  
  SHOW("Exception: integer->char : not an int",R0);
  return 1; 
 
