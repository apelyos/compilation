

L_APPLY:
  PUSH(FP);
  MOV(FP,SP);
  
  
  CMP(FPARG(1), IMM(2));
  JUMP_NE(L_APPLY_ERR_ARGS);
  MOV(R0, FPARG(2));
  CMP(INDD(R0,0), T_CLOSURE);
  JUMP_NE(L_APPLY_NOT_CLOSURE); 
  MOV(R0, FPARG(3));
  CMP(INDD(R0,0), T_NIL);
  JUMP_EQ(L_APPLY_CONTINUE);
  CMP(INDD(R0,0), T_PAIR);
  JUMP_NE(L_APPLY_NOT_LIST);
  
  L_APPLY_CONTINUE:
  /* push args to the stack - without reversing */
  MOV(R12, IMM(0));
  MOV(R0, FPARG(3));

L_APPLY_LOOP_PUSH_ARGS_START:
  CMP(INDD(R0,0), T_NIL);
  JUMP_EQ(L_APPLY_LOOP_PUSH_ARGS_END);
  
  PUSH(INDD(R0,1));
  MOV(R0, INDD(R0,2));
  INCR(R12);
  JUMP(L_APPLY_LOOP_PUSH_ARGS_START);

  
L_APPLY_LOOP_PUSH_ARGS_END:  
  PUSH(R12); /* push the number of arguments */
  MOV(R0, FPARG(2));
  PUSH(INDD(R0,1)); /* push the env */
  
  MOV(R1, STACK(FP-1));
  MOV(R2, STACK(FP-2));
  MOV(R3, FPARG(1));
  
L_OVERWRITE_FRAME:     
  MOV(R5, IMM(0));
  
L_OVERWRITE_FRAME_LOOP_START:
  CMP(R5, R12+2);
  JUMP_GE(L_OVERWRITE_FRAME_LOOP_END);
  MOV(FPARG(1+R3-R5), STACK(FP+R5));
  INCR(R5);
  JUMP(L_OVERWRITE_FRAME_LOOP_START);
  
L_OVERWRITE_FRAME_LOOP_END:
  DROP(R3+4);
  MOV(R5, IMM(0));
  MOV(R11, R12);
  SHR(R11, IMM(1));
  
L_REVERSE_ARGS_LOOP_START:
  CMP(R5, R11);
  JUMP_GE(L_REVERSE_ARGS_LOOP_END);
  MOV(R13, STACK(SP-3-R5));
  MOV(STACK(SP-3-R5), STACK(SP-2-R12+R5));
  MOV(STACK(SP-2-R12+R5), R13);
  INCR(R5);
  JUMP(L_REVERSE_ARGS_LOOP_START);  

L_REVERSE_ARGS_LOOP_END:
  PUSH(R2);
  MOV(FP, R1);
  JUMPA(INDD(R0,2));
  
  POP(R1);
  POP(R1);
  DROP(R1);

  
  L_APPLY_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (apply)",R0);
  return 1;

  L_APPLY_NOT_CLOSURE:  
  SHOW("Exception: the argument is not a closure",R0);
  return 1;
  
  L_APPLY_NOT_LIST:  
  SHOW("Exception: the argument is not a list",R0);
  return 1;
    
    
    POP(FP);
    RETURN;
  
  
  
