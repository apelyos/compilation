/*
  (RESPECT TO YOS'S IDEA)
  PROGRAMMER: NOT MAYER, BUT: YUVAL MAYMON
 */ 
 
 L_BOOLEAN:
  PUSH(FP);
  MOV(FP,SP);
  
  CMP(FPARG(1),IMM(1));
  JUMP_NE(L_BOOLEAN_ERR_ARGS);
  PUSH(FPARG(2));
  CALL(IS_SOB_BOOL);
  DROP(1);
  
  CALL(TRUE_OR_FALSE_IN_R0);
  
  POP(FP);
  RETURN;
  
 L_BOOLEAN_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (boolean?)",R0);
  return 1;