/* (vector-set! vec pos v) */
L_VECTOR_SET:
	PUSH(FP);
	MOV(FP, SP);
	
	CMP(FPARG(1), IMM(3));
	JUMP_NE(L_VECTOR_SET_IncorrectNumberOfArgs);
	MOV(R1, FPARG(2));
	CMP(INDD(R1, 0), IMM(T_VECTOR)); 
	JUMP_NE (L_VECTOR_SET_IncorrectNotAVector);
	MOV(R2, FPARG(3));
	CMP(INDD(R2, 0), IMM(T_INTEGER));
	JUMP_NE(L_VECTOR_SET_IncorrectIndex);
	MOV(R2, INDD(R2, 1));
	CMP(R2, INDD(R1, 1));
	JUMP_GE(L_VECTOR_SET_OutOfRange);
	MOV(R3, FPARG(4)); 
	ADD(R2, IMM(2)) ; 
	MOV(INDD(R1, R2), R3);
	MOV(R0, IMM(CONST_VOID_ADDR));

	POP(FP);
	RETURN;
	
L_VECTOR_SET_IncorrectNumberOfArgs:
	SHOW("vector-set! called with incorrect # of args!\n", R0);
	exit(1);
L_VECTOR_SET_IncorrectNotAVector:
	SHOW("vector-set! was called with a non-vector!\n", R0);
	exit(1);
L_VECTOR_SET_IncorrectIndex:
	SHOW("vector-set! was called with a non-integer index!\n", R0);
	exit(1);	
L_VECTOR_SET_OutOfRange:
	SHOW("vector-set! out of range!\n", R0);
	exit(1);
	