/* PROGRAMMER: NOT MAYER, BUT: YOS APEL
 */

 L_CDR:
  PUSH(FP);
  MOV(FP, SP);
  
  CMP(FPARG(1),IMM(1));
  JUMP_NE(L_CDR_ERR_ARGS);
  MOV(R0,FPARG(2));
  CMP(INDD(R0,0),IMM(T_PAIR));
  JUMP_NE(L_CDR_ERR_NOT_PAIR);
  MOV(R0,INDD(R0,2));
  
  POP(FP);
  RETURN;
  
L_CDR_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (cdr)",R0);
  return 1;
  
L_CDR_ERR_NOT_PAIR:
  SHOW("Exception in cdr: not a pair",R0);
  return 1;
  
L_CDR_EXIT:
