/*
  YOS
 */ 
 
 L_STR_TO_SYM:
  PUSH(FP);
  MOV(FP,SP);

  CMP(FPARG(1),IMM(1));
  JUMP_NE(L_STR_TO_SYM_ERR_ARGS);
  PUSH(FPARG(2));
  CALL(IS_SOB_STRING);
  DROP(1);
  
  CMP(R0,IMM(1));
  JUMP_NE(L_STR_TO_SYM_ERR_NOT_STRING);
  
  MOV(R1,FPARG(2)); /* str ptr */
  MOV(R2,IMM(STRING_LINKED_LIST_ADDR)); /* linked list */
  MOV(R3,CONST_NIL_ADDR); /* holds prev elem */
  
  L_STR_TO_SYM_LOOP:
  CMP(CONST_NIL_ADDR, ADDR(R2));
  JUMP_EQ(L_STR_TO_SYM_NOT_EXIST);
  /*printf("comparing strings %d %d\n",ADDR(R2),R1);*/
  /* CMP(ADDR(R2),R1); compare strings */
  PUSH(ADDR(R2));
  PUSH(R1);
  CALL(L_STRING_COMPARE);
  DROP(2);
  
  CMP(R0,IMM(1));
  JUMP_EQ(L_STR_TO_SYM_EXIST);
  MOV(R3,R2);
  MOV(R2,INDD(R2,1));
  JUMP(L_STR_TO_SYM_LOOP);
  
  
  L_STR_TO_SYM_EXIST:
  /* has ptr to found string in R2 */
  PUSH(ADDR(R2));
  CALL(MAKE_SOB_SYMBOL);
  DROP(1);
  JUMP(L_STR_TO_SYM_EXIT);
  
  /*printf("exist! in %d\n", R1);
  return 0;*/
  
  L_STR_TO_SYM_NOT_EXIST:
  /*need to add to linked list */  
  
  CMP(R3, CONST_NIL_ADDR);
  JUMP_EQ(L_STR_TO_SYM_LIST_EMPTY);
  
  /* list has some elem */
  PUSH(IMM(2));
  CALL(MALLOC);
  DROP(1);
  MOV(ADDR(R0), R1);
  MOV(INDD(R0,1), R2); /* R2 is nil elem */
  MOV(INDD(R3,1), R0);
  
  PUSH(R1);
  CALL(MAKE_SOB_SYMBOL);
  DROP(1);
  JUMP(L_STR_TO_SYM_EXIT);
  
  L_STR_TO_SYM_LIST_EMPTY:
  /* list is completely empty */
  PUSH(IMM(2));
  CALL(MALLOC);
  DROP(1);
  MOV(ADDR(R0), IMM(CONST_NIL_ADDR));
  MOV(INDD(R0,1), IMM(CONST_NIL_ADDR));
  MOV(R4, IMM(STRING_LINKED_LIST_ADDR));
  MOV(ADDR(R4), R1);
  MOV(INDD(R4,1), R0);
  
  PUSH(R1);
  CALL(MAKE_SOB_SYMBOL);
  DROP(1);
  JUMP(L_STR_TO_SYM_EXIT);
  
  /*printf("not exist!\n");
  return 0; */
  
  
  L_STR_TO_SYM_EXIT:
  POP(FP);
  RETURN;
  
 L_STR_TO_SYM_ERR_NOT_STRING:
  SHOW("Error: Parameter must be a string",R0);
  return 1;
  
 L_STR_TO_SYM_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (string->symbol)",R0);
  return 1; 
 
 
 
 
