/*
  (RESPECT TO YOS'S IDEA)
  PROGRAMMER: NOT MAYER, BUT: YUVAL MAYMON
 */ 
 
 L_VECTOR:
  PUSH(FP);
  MOV(FP,SP);

  CMP(FPARG(1),IMM(1));
  JUMP_NE(L_VECTOR_ERR_ARGS);
  PUSH(FPARG(2));
  CALL(IS_SOB_VECTOR);
  DROP(1);
  
  CALL(TRUE_OR_FALSE_IN_R0);
  
  POP(FP);
  RETURN;
  
 L_VECTOR_ERR_ARGS:  
  SHOW("Exception: incorrect argument count in call (vector?)",R0);
  return 1; 
 
 
 
 
 
 
