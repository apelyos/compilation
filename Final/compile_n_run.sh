# make sure you have "test.scm"
echo "#use \"compiler.ml\";;
compile_scheme_file \"test.scm\" \"out_test.c\";;" | ocaml -stdin
gcc -o out_test out_test.c
echo "Our output:"
our=`./out_test`
echo $our
echo "Petite output:"
petite=`cat test.scm | petite -q`
echo $petite
echo "equal?"
result=""
if [ "$our" = "$petite" ]; then
  result="#t"
else
  result=`echo "(equal? '($our) '($petite))" | petite -q`
fi

echo $result
if [ "$result" = "#f" ]; then 
    echo "-> RESULTS DIFFER - WE HAVE A BUG!!!"
fi
