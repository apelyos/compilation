(define list (lambda s s))

(define not (lambda (x) (if x #f #t)))

(define map
  (let* ((y-comb
	  (lambda (f) 
	    (let ((fun1
		      (lambda (x) 
			(f (lambda (y z)
			((x x) y z))))))
		(fun1 fun1))))
		
	  (y-comb-map1
	    (y-comb (lambda (map1) 
		      (lambda (f s) 
			(if (null? s) '() 
			    (cons (f (car s)) 
				  (map1 f (cdr s))))))))

	  (y-comb-maplist
	    (y-comb (lambda (maplist) 
		      (lambda (f s) 
			(if (null? (car s)) 
			    '() 
			    (cons (apply f (y-comb-map1 car s)) 
				  (maplist f (y-comb-map1 cdr s)))))))))

	(lambda (f . s) 
	  (y-comb-maplist f s))))



(define append_helper 
  (lambda (l1 l2)
    (if (null? l1)
      l2
      (cons (car l1) (append_helper (cdr l1) l2)))))
      
(define append_helper_rest 
  (lambda (first rest)
    (if (null? rest)
      first
      (append_helper first (append_helper_rest (car rest) (cdr rest))))))
      
(define append 
  (lambda s
    (if (null? s)
      s
      (append_helper_rest (car s) (cdr s)))))


(define arith-helper 
  (lambda (op neutral lst)
    (if (null? lst)
      neutral
	(op (car lst) (arith-helper op neutral (cdr lst))))))

(define + 
  (lambda args
    (arith-helper add 0 args)))	

(define -
  (lambda args
  (if (null? (cdr args))
    (subtract 0 (car args))
    (subtract (car args) (arith-helper add 0 (cdr args))))))
	
(define * 
  (lambda args
    (arith-helper multiply 1 args)))
    
(define /
  (lambda args
  (if (null? (cdr args))
    (divide 1 (car args))  
    (divide (car args) (arith-helper multiply 1 (cdr args))))))
    
(define comparison-helper
  (lambda (op first rest)
    (if (null? rest)
      #t
      (and (op first (car rest)) (comparison-helper op (car rest) (cdr rest))))))

(define eq-helper
  (lambda (x y)
    (zero? (- x y))))
    
(define lt-helper
  (lambda (x y)
    (arith-lt-zero (- x y))))
    
(define gt-helper
  (lambda (x y)
    (arith-gt-zero (- x y))))
    
(define =
  (lambda args
    (comparison-helper eq-helper (car args) (cdr args))))    
    
(define <
  (lambda args
    (comparison-helper lt-helper (car args) (cdr args))))

(define >
  (lambda args
    (comparison-helper gt-helper (car args) (cdr args))))



