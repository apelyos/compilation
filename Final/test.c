
#include <stdio.h>
#include <stdlib.h>

#define DO_SHOW 1 

#include "cisc.h"

int main()
{
  START_MACHINE;

/* const memory load */
MOV(ADDR(10),IMM(937610));
MOV(ADDR(11),IMM(722689));
MOV(ADDR(12),IMM(741553));
MOV(ADDR(13),IMM(0));
MOV(ADDR(14),IMM(741553));
MOV(ADDR(15),IMM(1));
/* end const memory load */

/* str_linked_list memory load */
MOV(ADDR(35),IMM(11));
/* end list memory load */


  JUMP(CONTINUE);

#define CONST_FALSE_ADDR 12
#define CONST_TRUE_ADDR 14
#define CONST_NIL_ADDR 11
#define CONST_VOID_ADDR 10
#define STRING_LINKED_LIST_ADDR 35
#define USABLE_MEM_START 37
#include "char.lib"
#include "io.lib"
#include "math.lib"
#include "string.lib"
#include "system.lib"
#include "scheme.lib"
#include "procedures.lib"

 CONTINUE:
/* set mem start of mallocs (usable memory) */
MOV(ADDR(0),IMM(USABLE_MEM_START));

/* lib closure generation */
PUSH(LABEL(L_CAR));
PUSH(IMM(496351));
CALL(MAKE_SOB_CLOSURE);
DROP(2);
MOV(IND(17),R0);
PUSH(LABEL(L_CDR));
PUSH(IMM(496351));
CALL(MAKE_SOB_CLOSURE);
DROP(2);
MOV(IND(18),R0);
PUSH(LABEL(L_CONS));
PUSH(IMM(496351));
CALL(MAKE_SOB_CLOSURE);
DROP(2);
MOV(IND(19),R0);
PUSH(LABEL(L_EQ));
PUSH(IMM(496351));
CALL(MAKE_SOB_CLOSURE);
DROP(2);
MOV(IND(20),R0);
PUSH(LABEL(L_BOOLEAN));
PUSH(IMM(496351));
CALL(MAKE_SOB_CLOSURE);
DROP(2);
MOV(IND(21),R0);
PUSH(LABEL(L_CHAR));
PUSH(IMM(496351));
CALL(MAKE_SOB_CLOSURE);
DROP(2);
MOV(IND(22),R0);
PUSH(LABEL(L_INTEGER));
PUSH(IMM(496351));
CALL(MAKE_SOB_CLOSURE);
DROP(2);
MOV(IND(23),R0);
PUSH(LABEL(L_NULL));
PUSH(IMM(496351));
CALL(MAKE_SOB_CLOSURE);
DROP(2);
MOV(IND(24),R0);
PUSH(LABEL(L_PAIR));
PUSH(IMM(496351));
CALL(MAKE_SOB_CLOSURE);
DROP(2);
MOV(IND(25),R0);
PUSH(LABEL(L_PROCEDURE));
PUSH(IMM(496351));
CALL(MAKE_SOB_CLOSURE);
DROP(2);
MOV(IND(26),R0);
PUSH(LABEL(L_STRING));
PUSH(IMM(496351));
CALL(MAKE_SOB_CLOSURE);
DROP(2);
MOV(IND(27),R0);
PUSH(LABEL(L_SYMBOL));
PUSH(IMM(496351));
CALL(MAKE_SOB_CLOSURE);
DROP(2);
MOV(IND(28),R0);
PUSH(LABEL(L_VECTOR));
PUSH(IMM(496351));
CALL(MAKE_SOB_CLOSURE);
DROP(2);
MOV(IND(29),R0);
PUSH(LABEL(L_RATIONAL));
PUSH(IMM(496351));
CALL(MAKE_SOB_CLOSURE);
DROP(2);
MOV(IND(30),R0);
PUSH(LABEL(L_RATIONAL));
PUSH(IMM(496351));
CALL(MAKE_SOB_CLOSURE);
DROP(2);
MOV(IND(31),R0);
/* end lib generation */

/* start of code gen */
/* generating def with freevar: list */
/* generating Lambda-Opt of params: with optinal parameter: s */
/* push all the registers that we are going to use */
PUSH(R1);
PUSH(R2);
PUSH(R3);
PUSH(IMM(0));    /* enlarge the env */
CALL(MALLOC);
DROP(1);
MOV(R1, R0);    /* R1 holds the address of the new env */
MOV(R2, FPARG(0));    /* R2 holds the env */
/* synchronize between R1 and R2 (the environments) */
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(1));
MOV(R5, IMM(0));
L_LOOP_6:
CMP(R5,0);
JUMP_EQ(L_LOOP_END_6);
MOV(INDD(R1,R4), INDD(R2,R5));
INCR(R4);
INCR(R5);
JUMP(L_LOOP_6);
L_LOOP_END_6:
POP(R5);
POP(R4);
/* add the last closure to env[0] */
PUSH(FPARG(1));
CALL(MALLOC);
DROP(1);
MOV(R3, R0);
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(0));
MOV(R5, FPARG(1));
L_LOOP_5:
CMP(R4,R5);
JUMP_EQ(L_LOOP_END_5);
MOV(INDD(R3,R4), FPARG(2+R4));
INCR(R4);
JUMP(L_LOOP_5);
L_LOOP_END_5:
POP(R5);
POP(R4);
MOV(INDD(R1,0), R3);
/* construct the new closure and put it in R0 */
PUSH(IMM(3));
CALL(MALLOC);
DROP(1);
MOV(INDD(R0,0), IMM(T_CLOSURE));
MOV(INDD(R0,1), R1);
MOV(INDD(R0,2), LABEL(L_BODY_1));
JUMP(L_EXIT_1);
/* the **BODY** of the Lambda-Opt of params: with optinal parameter: s */
L_BODY_1:
CMP(STACK(SP-3),0);    /* compare the number of params with the number of arguments */
JUMP_EQ(L_ENLARGE_ENV_1);
/* reduce the env */
MOV(R2,SP);
MOV(R3,STACK(SP-3));    /* R3 holds the number of the arguments */
MOV(R6, R3-0);    /* R6 holds the number od the optional params */
PUSH(R0);
CALL(MAKE_SOB_NIL);
MOV(R1,R0);
POP(R0);
/* make a list out of the optional arguments */
MOV(R5, STACK(SP-3));
L_LOOP_4:
CMP(R5,0);
JUMP_EQ(L_LOOP_END_4);
/* make pair */
PUSH(R0);
PUSH(R1);
PUSH(STACK(R2-3-R5));
CALL(MAKE_SOB_PAIR);
DROP(2);
MOV(R1, R0);
POP(R0);
DECR(R5);
JUMP(L_LOOP_4);
L_LOOP_END_4:
MOV(STACK(R2-3-R3), R1);
MOV(R7, STACK(SP-2));    /* R7 holds the env */
MOV(R8, STACK(SP-1));    /* R8 holds the return address */
/* step down the arguments */
MOV(R5, 0);
L_LOOP_3:
CMP(R5,IMM(0));
JUMP_EQ(L_LOOP_END_3);
MOV(STACK(R2-2-R3+0-R5), STACK(R2-3-R5));
DECR(R5);
JUMP(L_LOOP_3);
L_LOOP_END_3:
DROP(R6-1+3);    /* update SP */
PUSH(0+1);    /* update number of arguments */
PUSH(R7);    /* push back the env */
PUSH(R8);    /* push back the return address */
JUMP(L_CONTINUE_BODY_OPT_1);
L_ENLARGE_ENV_1:
MOV(R1,SP);
PUSH(STACK(R1-1));    /* step up the return address */
MOV(STACK(R1-1), STACK(R1-2));    /* step up the env */
MOV(STACK(R1-2), 0+1);    /* increment by 1 the number of arguments (params) */
/* step up the arguments */
MOV(R5, IMM(0));
L_LOOP_2:
CMP(R5,0);
JUMP_EQ(L_LOOP_END_2);
MOV(STACK(R1-3-R5), STACK(R1-4-R5));
INCR(R5);
JUMP(L_LOOP_2);
L_LOOP_END_2:
PUSH(R0);
CALL(MAKE_SOB_NIL);
MOV(STACK(R1-3-0), R0);    /* insert NIL as the optional parameter */
POP(R0);
L_CONTINUE_BODY_OPT_1:
PUSH(FP);
MOV(FP,SP);
/* generating varParam for s  with minor: 0 */
MOV(R0, FPARG(2));

POP(FP);
RETURN;
L_EXIT_1:
POP(R3);
POP(R2);
POP(R1);
/* END of the generating of Lambda-Opt */
MOV(IND(32), R0);
MOV(R0,10);
/* print result in r0 */
CALL(WRITE_R0_RESULT);

/* generating def with freevar: not */
/* generating Lambda-Simple of params: x */
/* push all the registers that we are going to use */
PUSH(R1);
PUSH(R2);
PUSH(R3);
PUSH(IMM(0));    /* enlarge the env */
CALL(MALLOC);
DROP(1);
MOV(R1, R0);    /* R1 holds the address of the new env */
MOV(R2, FPARG(0));    /* R2 holds the env */
/* synchronize between R1 and R2 (the environments) */
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(1));
MOV(R5, IMM(0));
L_LOOP_10:
CMP(R5,0);
JUMP_EQ(L_LOOP_END_10);
MOV(INDD(R1,R4), INDD(R2,R5));
INCR(R4);
INCR(R5);
JUMP(L_LOOP_10);
L_LOOP_END_10:
POP(R5);
POP(R4);
/* add the last closure to env[0] */
PUSH(FPARG(1));
CALL(MALLOC);
DROP(1);
MOV(R3, R0);
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(0));
MOV(R5, FPARG(1));
L_LOOP_9:
CMP(R4,R5);
JUMP_EQ(L_LOOP_END_9);
MOV(INDD(R3,R4), FPARG(2+R4));
INCR(R4);
JUMP(L_LOOP_9);
L_LOOP_END_9:
POP(R5);
POP(R4);
MOV(INDD(R1,0), R3);
/* construct the new closure and put it in R0 */
PUSH(IMM(3));
CALL(MALLOC);
DROP(1);
MOV(INDD(R0,0), IMM(T_CLOSURE));
MOV(INDD(R0,1), R1);
MOV(INDD(R0,2), LABEL(L_BODY_7));
JUMP(L_EXIT_7);
/* the **BODY** of the Lambda-Simple of params: x */
L_BODY_7:
PUSH(FP);
MOV(FP,SP);
/* generating if */
/* generating varParam for x  with minor: 0 */
MOV(R0, FPARG(2));

CMP(R0, IMM(12));
JUMP_EQ(L_IF_ELSE_8);
/* getting const of #f */
MOV(R0,IMM(12));

JUMP(L_IF_EXIT_8);
L_IF_ELSE_8:
/* getting const of #t */
MOV(R0,IMM(14));

L_IF_EXIT_8:
POP(FP);
RETURN;
L_EXIT_7:
POP(R3);
POP(R2);
POP(R1);
/* END of the generating of Lambda-Simple */
MOV(IND(33), R0);
MOV(R0,10);
/* print result in r0 */
CALL(WRITE_R0_RESULT);

/* generating Applic */
/* push args to the stack */
/* generating Lambda-Simple of params: x, y */
/* push all the registers that we are going to use */
PUSH(R1);
PUSH(R2);
PUSH(R3);
PUSH(IMM(0));    /* enlarge the env */
CALL(MALLOC);
DROP(1);
MOV(R1, R0);    /* R1 holds the address of the new env */
MOV(R2, FPARG(0));    /* R2 holds the env */
/* synchronize between R1 and R2 (the environments) */
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(1));
MOV(R5, IMM(0));
L_LOOP_35:
CMP(R5,0);
JUMP_EQ(L_LOOP_END_35);
MOV(INDD(R1,R4), INDD(R2,R5));
INCR(R4);
INCR(R5);
JUMP(L_LOOP_35);
L_LOOP_END_35:
POP(R5);
POP(R4);
/* add the last closure to env[0] */
PUSH(FPARG(1));
CALL(MALLOC);
DROP(1);
MOV(R3, R0);
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(0));
MOV(R5, FPARG(1));
L_LOOP_34:
CMP(R4,R5);
JUMP_EQ(L_LOOP_END_34);
MOV(INDD(R3,R4), FPARG(2+R4));
INCR(R4);
JUMP(L_LOOP_34);
L_LOOP_END_34:
POP(R5);
POP(R4);
MOV(INDD(R1,0), R3);
/* construct the new closure and put it in R0 */
PUSH(IMM(3));
CALL(MALLOC);
DROP(1);
MOV(INDD(R0,0), IMM(T_CLOSURE));
MOV(INDD(R0,1), R1);
MOV(INDD(R0,2), LABEL(L_BODY_33));
JUMP(L_EXIT_33);
/* the **BODY** of the Lambda-Simple of params: x, y */
L_BODY_33:
PUSH(FP);
MOV(FP,SP);
/* generating varParam for x  with minor: 0 */
MOV(R0, FPARG(2));

POP(FP);
RETURN;
L_EXIT_33:
POP(R3);
POP(R2);
POP(R1);
/* END of the generating of Lambda-Simple */
PUSH(R0);
PUSH(1);    /* push the number of arguments */
/* the code of the relevant procedure of the applic: */
/* generating Applic */
/* push args to the stack */
/* generating Lambda-Simple of params: z */
/* push all the registers that we are going to use */
PUSH(R1);
PUSH(R2);
PUSH(R3);
PUSH(IMM(0));    /* enlarge the env */
CALL(MALLOC);
DROP(1);
MOV(R1, R0);    /* R1 holds the address of the new env */
MOV(R2, FPARG(0));    /* R2 holds the env */
/* synchronize between R1 and R2 (the environments) */
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(1));
MOV(R5, IMM(0));
L_LOOP_32:
CMP(R5,0);
JUMP_EQ(L_LOOP_END_32);
MOV(INDD(R1,R4), INDD(R2,R5));
INCR(R4);
INCR(R5);
JUMP(L_LOOP_32);
L_LOOP_END_32:
POP(R5);
POP(R4);
/* add the last closure to env[0] */
PUSH(FPARG(1));
CALL(MALLOC);
DROP(1);
MOV(R3, R0);
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(0));
MOV(R5, FPARG(1));
L_LOOP_31:
CMP(R4,R5);
JUMP_EQ(L_LOOP_END_31);
MOV(INDD(R3,R4), FPARG(2+R4));
INCR(R4);
JUMP(L_LOOP_31);
L_LOOP_END_31:
POP(R5);
POP(R4);
MOV(INDD(R1,0), R3);
/* construct the new closure and put it in R0 */
PUSH(IMM(3));
CALL(MALLOC);
DROP(1);
MOV(INDD(R0,0), IMM(T_CLOSURE));
MOV(INDD(R0,1), R1);
MOV(INDD(R0,2), LABEL(L_BODY_29));
JUMP(L_EXIT_29);
/* the **BODY** of the Lambda-Simple of params: z */
L_BODY_29:
PUSH(FP);
MOV(FP,SP);
/* generating ApplicTP */
/* push args to the stack */
/* getting const of #f */
MOV(R0,IMM(12));
PUSH(R0);
/* getting const of #t */
MOV(R0,IMM(14));
PUSH(R0);
PUSH(2);    /* push the number of arguments */
/* the code of the relevant procedure of the applic: */
/* generating varParam for z  with minor: 0 */
MOV(R0, FPARG(2));
/* check if the procedure is a valid procedure - if not, throw an exception */
CMP(INDD(R0,0), IMM(T_CLOSURE));
JUMP_NE(L_EXCEPTION_NOT_A_PROCEDURE);
PUSH(INDD(R0,1));    /* push the environment */
MOV(R1,STACK(FP-1));    /* mov R1 the old fp */
MOV(R2,STACK(FP-2));    /* mov R2 the return address */
/* overwrite the last frame: */
MOV(R5, IMM(0));
L_LOOP_30:
CMP(R5,2+2);
JUMP_EQ(L_LOOP_END_30);
MOV(STACK(R1+R5),STACK(FP+R5));
INCR(R5);
JUMP(L_LOOP_30);
L_LOOP_END_30:

MOV(SP, R1+2+2);
PUSH(R2);    /* push old return address */
MOV(FP,R1);
JUMPA(INDD(R0,2));    /* apply the body of the procedure */
POP(R1);
POP(R1);
DROP(R1);    /* drop the parameters */

POP(FP);
RETURN;
L_EXIT_29:
POP(R3);
POP(R2);
POP(R1);
/* END of the generating of Lambda-Simple */
PUSH(R0);
PUSH(1);    /* push the number of arguments */
/* the code of the relevant procedure of the applic: */
/* generating Applic */
/* push args to the stack */
/* generating Lambda-Simple of params: p */
/* push all the registers that we are going to use */
PUSH(R1);
PUSH(R2);
PUSH(R3);
PUSH(IMM(0));    /* enlarge the env */
CALL(MALLOC);
DROP(1);
MOV(R1, R0);    /* R1 holds the address of the new env */
MOV(R2, FPARG(0));    /* R2 holds the env */
/* synchronize between R1 and R2 (the environments) */
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(1));
MOV(R5, IMM(0));
L_LOOP_28:
CMP(R5,0);
JUMP_EQ(L_LOOP_END_28);
MOV(INDD(R1,R4), INDD(R2,R5));
INCR(R4);
INCR(R5);
JUMP(L_LOOP_28);
L_LOOP_END_28:
POP(R5);
POP(R4);
/* add the last closure to env[0] */
PUSH(FPARG(1));
CALL(MALLOC);
DROP(1);
MOV(R3, R0);
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(0));
MOV(R5, FPARG(1));
L_LOOP_27:
CMP(R4,R5);
JUMP_EQ(L_LOOP_END_27);
MOV(INDD(R3,R4), FPARG(2+R4));
INCR(R4);
JUMP(L_LOOP_27);
L_LOOP_END_27:
POP(R5);
POP(R4);
MOV(INDD(R1,0), R3);
/* construct the new closure and put it in R0 */
PUSH(IMM(3));
CALL(MALLOC);
DROP(1);
MOV(INDD(R0,0), IMM(T_CLOSURE));
MOV(INDD(R0,1), R1);
MOV(INDD(R0,2), LABEL(L_BODY_18));
JUMP(L_EXIT_18);
/* the **BODY** of the Lambda-Simple of params: p */
L_BODY_18:
PUSH(FP);
MOV(FP,SP);
/* generating ApplicTP */
/* push args to the stack */
/* generating Lambda-Simple of params: x, y */
/* push all the registers that we are going to use */
PUSH(R1);
PUSH(R2);
PUSH(R3);
PUSH(IMM(1));    /* enlarge the env */
CALL(MALLOC);
DROP(1);
MOV(R1, R0);    /* R1 holds the address of the new env */
MOV(R2, FPARG(0));    /* R2 holds the env */
/* synchronize between R1 and R2 (the environments) */
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(1));
MOV(R5, IMM(0));
L_LOOP_26:
CMP(R5,1);
JUMP_EQ(L_LOOP_END_26);
MOV(INDD(R1,R4), INDD(R2,R5));
INCR(R4);
INCR(R5);
JUMP(L_LOOP_26);
L_LOOP_END_26:
POP(R5);
POP(R4);
/* add the last closure to env[0] */
PUSH(FPARG(1));
CALL(MALLOC);
DROP(1);
MOV(R3, R0);
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(0));
MOV(R5, FPARG(1));
L_LOOP_25:
CMP(R4,R5);
JUMP_EQ(L_LOOP_END_25);
MOV(INDD(R3,R4), FPARG(2+R4));
INCR(R4);
JUMP(L_LOOP_25);
L_LOOP_END_25:
POP(R5);
POP(R4);
MOV(INDD(R1,0), R3);
/* construct the new closure and put it in R0 */
PUSH(IMM(3));
CALL(MALLOC);
DROP(1);
MOV(INDD(R0,0), IMM(T_CLOSURE));
MOV(INDD(R0,1), R1);
MOV(INDD(R0,2), LABEL(L_BODY_20));
JUMP(L_EXIT_20);
/* the **BODY** of the Lambda-Simple of params: x, y */
L_BODY_20:
PUSH(FP);
MOV(FP,SP);
/* generating Lambda-Simple of params: p */
/* push all the registers that we are going to use */
PUSH(R1);
PUSH(R2);
PUSH(R3);
PUSH(IMM(2));    /* enlarge the env */
CALL(MALLOC);
DROP(1);
MOV(R1, R0);    /* R1 holds the address of the new env */
MOV(R2, FPARG(0));    /* R2 holds the env */
/* synchronize between R1 and R2 (the environments) */
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(1));
MOV(R5, IMM(0));
L_LOOP_24:
CMP(R5,2);
JUMP_EQ(L_LOOP_END_24);
MOV(INDD(R1,R4), INDD(R2,R5));
INCR(R4);
INCR(R5);
JUMP(L_LOOP_24);
L_LOOP_END_24:
POP(R5);
POP(R4);
/* add the last closure to env[0] */
PUSH(FPARG(1));
CALL(MALLOC);
DROP(1);
MOV(R3, R0);
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(0));
MOV(R5, FPARG(1));
L_LOOP_23:
CMP(R4,R5);
JUMP_EQ(L_LOOP_END_23);
MOV(INDD(R3,R4), FPARG(2+R4));
INCR(R4);
JUMP(L_LOOP_23);
L_LOOP_END_23:
POP(R5);
POP(R4);
MOV(INDD(R1,0), R3);
/* construct the new closure and put it in R0 */
PUSH(IMM(3));
CALL(MALLOC);
DROP(1);
MOV(INDD(R0,0), IMM(T_CLOSURE));
MOV(INDD(R0,1), R1);
MOV(INDD(R0,2), LABEL(L_BODY_21));
JUMP(L_EXIT_21);
/* the **BODY** of the Lambda-Simple of params: p */
L_BODY_21:
PUSH(FP);
MOV(FP,SP);
/* generating ApplicTP */
/* push args to the stack */
/* generating varBound for x with major: 0 minor: 0 */
MOV(R0, FPARG(0));
MOV(R0, INDD(R0, 0));
MOV(R0, INDD(R0, 0));
PUSH(R0);
/* generating varBound for y with major: 0 minor: 1 */
MOV(R0, FPARG(0));
MOV(R0, INDD(R0, 0));
MOV(R0, INDD(R0, 1));
PUSH(R0);
PUSH(2);    /* push the number of arguments */
/* the code of the relevant procedure of the applic: */
/* generating varParam for p  with minor: 0 */
MOV(R0, FPARG(2));
/* check if the procedure is a valid procedure - if not, throw an exception */
CMP(INDD(R0,0), IMM(T_CLOSURE));
JUMP_NE(L_EXCEPTION_NOT_A_PROCEDURE);
PUSH(INDD(R0,1));    /* push the environment */
MOV(R1,STACK(FP-1));    /* mov R1 the old fp */
MOV(R2,STACK(FP-2));    /* mov R2 the return address */
/* overwrite the last frame: */
MOV(R5, IMM(0));
L_LOOP_22:
CMP(R5,2+2);
JUMP_EQ(L_LOOP_END_22);
MOV(STACK(R1+R5),STACK(FP+R5));
INCR(R5);
JUMP(L_LOOP_22);
L_LOOP_END_22:

MOV(SP, R1+2+2);
PUSH(R2);    /* push old return address */
MOV(FP,R1);
JUMPA(INDD(R0,2));    /* apply the body of the procedure */
POP(R1);
POP(R1);
DROP(R1);    /* drop the parameters */

POP(FP);
RETURN;
L_EXIT_21:
POP(R3);
POP(R2);
POP(R1);
/* END of the generating of Lambda-Simple */

POP(FP);
RETURN;
L_EXIT_20:
POP(R3);
POP(R2);
POP(R1);
/* END of the generating of Lambda-Simple */
PUSH(R0);
PUSH(1);    /* push the number of arguments */
/* the code of the relevant procedure of the applic: */
/* generating varParam for p  with minor: 0 */
MOV(R0, FPARG(2));
/* check if the procedure is a valid procedure - if not, throw an exception */
CMP(INDD(R0,0), IMM(T_CLOSURE));
JUMP_NE(L_EXCEPTION_NOT_A_PROCEDURE);
PUSH(INDD(R0,1));    /* push the environment */
MOV(R1,STACK(FP-1));    /* mov R1 the old fp */
MOV(R2,STACK(FP-2));    /* mov R2 the return address */
/* overwrite the last frame: */
MOV(R5, IMM(0));
L_LOOP_19:
CMP(R5,1+2);
JUMP_EQ(L_LOOP_END_19);
MOV(STACK(R1+R5),STACK(FP+R5));
INCR(R5);
JUMP(L_LOOP_19);
L_LOOP_END_19:

MOV(SP, R1+1+2);
PUSH(R2);    /* push old return address */
MOV(FP,R1);
JUMPA(INDD(R0,2));    /* apply the body of the procedure */
POP(R1);
POP(R1);
DROP(R1);    /* drop the parameters */

POP(FP);
RETURN;
L_EXIT_18:
POP(R3);
POP(R2);
POP(R1);
/* END of the generating of Lambda-Simple */
PUSH(R0);
PUSH(1);    /* push the number of arguments */
/* the code of the relevant procedure of the applic: */
/* generating Lambda-Simple of params: x */
/* push all the registers that we are going to use */
PUSH(R1);
PUSH(R2);
PUSH(R3);
PUSH(IMM(0));    /* enlarge the env */
CALL(MALLOC);
DROP(1);
MOV(R1, R0);    /* R1 holds the address of the new env */
MOV(R2, FPARG(0));    /* R2 holds the env */
/* synchronize between R1 and R2 (the environments) */
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(1));
MOV(R5, IMM(0));
L_LOOP_17:
CMP(R5,0);
JUMP_EQ(L_LOOP_END_17);
MOV(INDD(R1,R4), INDD(R2,R5));
INCR(R4);
INCR(R5);
JUMP(L_LOOP_17);
L_LOOP_END_17:
POP(R5);
POP(R4);
/* add the last closure to env[0] */
PUSH(FPARG(1));
CALL(MALLOC);
DROP(1);
MOV(R3, R0);
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(0));
MOV(R5, FPARG(1));
L_LOOP_16:
CMP(R4,R5);
JUMP_EQ(L_LOOP_END_16);
MOV(INDD(R3,R4), FPARG(2+R4));
INCR(R4);
JUMP(L_LOOP_16);
L_LOOP_END_16:
POP(R5);
POP(R4);
MOV(INDD(R1,0), R3);
/* construct the new closure and put it in R0 */
PUSH(IMM(3));
CALL(MALLOC);
DROP(1);
MOV(INDD(R0,0), IMM(T_CLOSURE));
MOV(INDD(R0,1), R1);
MOV(INDD(R0,2), LABEL(L_BODY_11));
JUMP(L_EXIT_11);
/* the **BODY** of the Lambda-Simple of params: x */
L_BODY_11:
PUSH(FP);
MOV(FP,SP);
/* generating Lambda-Simple of params: y */
/* push all the registers that we are going to use */
PUSH(R1);
PUSH(R2);
PUSH(R3);
PUSH(IMM(1));    /* enlarge the env */
CALL(MALLOC);
DROP(1);
MOV(R1, R0);    /* R1 holds the address of the new env */
MOV(R2, FPARG(0));    /* R2 holds the env */
/* synchronize between R1 and R2 (the environments) */
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(1));
MOV(R5, IMM(0));
L_LOOP_15:
CMP(R5,1);
JUMP_EQ(L_LOOP_END_15);
MOV(INDD(R1,R4), INDD(R2,R5));
INCR(R4);
INCR(R5);
JUMP(L_LOOP_15);
L_LOOP_END_15:
POP(R5);
POP(R4);
/* add the last closure to env[0] */
PUSH(FPARG(1));
CALL(MALLOC);
DROP(1);
MOV(R3, R0);
PUSH(R4);
PUSH(R5);
MOV(R4, IMM(0));
MOV(R5, FPARG(1));
L_LOOP_14:
CMP(R4,R5);
JUMP_EQ(L_LOOP_END_14);
MOV(INDD(R3,R4), FPARG(2+R4));
INCR(R4);
JUMP(L_LOOP_14);
L_LOOP_END_14:
POP(R5);
POP(R4);
MOV(INDD(R1,0), R3);
/* construct the new closure and put it in R0 */
PUSH(IMM(3));
CALL(MALLOC);
DROP(1);
MOV(INDD(R0,0), IMM(T_CLOSURE));
MOV(INDD(R0,1), R1);
MOV(INDD(R0,2), LABEL(L_BODY_12));
JUMP(L_EXIT_12);
/* the **BODY** of the Lambda-Simple of params: y */
L_BODY_12:
PUSH(FP);
MOV(FP,SP);
/* generating ApplicTP */
/* push args to the stack */
/* generating varParam for y  with minor: 0 */
MOV(R0, FPARG(2));
PUSH(R0);
PUSH(1);    /* push the number of arguments */
/* the code of the relevant procedure of the applic: */
/* generating varBound for x with major: 0 minor: 0 */
MOV(R0, FPARG(0));
MOV(R0, INDD(R0, 0));
MOV(R0, INDD(R0, 0));
/* check if the procedure is a valid procedure - if not, throw an exception */
CMP(INDD(R0,0), IMM(T_CLOSURE));
JUMP_NE(L_EXCEPTION_NOT_A_PROCEDURE);
PUSH(INDD(R0,1));    /* push the environment */
MOV(R1,STACK(FP-1));    /* mov R1 the old fp */
MOV(R2,STACK(FP-2));    /* mov R2 the return address */
/* overwrite the last frame: */
MOV(R5, IMM(0));
L_LOOP_13:
CMP(R5,1+2);
JUMP_EQ(L_LOOP_END_13);
MOV(STACK(R1+R5),STACK(FP+R5));
INCR(R5);
JUMP(L_LOOP_13);
L_LOOP_END_13:

MOV(SP, R1+1+2);
PUSH(R2);    /* push old return address */
MOV(FP,R1);
JUMPA(INDD(R0,2));    /* apply the body of the procedure */
POP(R1);
POP(R1);
DROP(R1);    /* drop the parameters */

POP(FP);
RETURN;
L_EXIT_12:
POP(R3);
POP(R2);
POP(R1);
/* END of the generating of Lambda-Simple */

POP(FP);
RETURN;
L_EXIT_11:
POP(R3);
POP(R2);
POP(R1);
/* END of the generating of Lambda-Simple */
/* check if the procedure is a valid procedure - if not, throw an exception */
CMP(INDD(R0,0), IMM(T_CLOSURE));
JUMP_NE(L_EXCEPTION_NOT_A_PROCEDURE);
PUSH(INDD(R0,1));    /* push the environment */
CALLA(INDD(R0,2));    /* apply the body of the procedure */
POP(R1);
POP(R1);
DROP(R1);    /* drop the parameters */
/* check if the procedure is a valid procedure - if not, throw an exception */
CMP(INDD(R0,0), IMM(T_CLOSURE));
JUMP_NE(L_EXCEPTION_NOT_A_PROCEDURE);
PUSH(INDD(R0,1));    /* push the environment */
CALLA(INDD(R0,2));    /* apply the body of the procedure */
POP(R1);
POP(R1);
DROP(R1);    /* drop the parameters */
/* check if the procedure is a valid procedure - if not, throw an exception */
CMP(INDD(R0,0), IMM(T_CLOSURE));
JUMP_NE(L_EXCEPTION_NOT_A_PROCEDURE);
PUSH(INDD(R0,1));    /* push the environment */
CALLA(INDD(R0,2));    /* apply the body of the procedure */
POP(R1);
POP(R1);
DROP(R1);    /* drop the parameters */
/* print result in r0 */
CALL(WRITE_R0_RESULT);


    
  /* end of code gen */
  STOP_MACHINE;
  return 0;

  L_EXCEPTION_NOT_A_PROCEDURE:
    CALL(MAKE_SOB_VOID);
    SHOW("Exception: attempt to apply non-procedure", R0);
    return 1;   
}
